import React from 'react';
import {Redirect} from "react-router-dom";

const SecretPage = ({isAuth}) => {
	const content = isAuth ? 'Secret page !!!!' : <Redirect to='/login'/>
	return (
		<div>
			<h1>{content}</h1>
		</div>
	);
};

export default SecretPage;