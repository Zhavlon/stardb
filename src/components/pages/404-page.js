import React from 'react';

const NotFound = () => {
	return (
		<div>
			<h1 style={{fontSize: '40px'}}>404 <br/> Not found</h1>
		</div>
	);
};

export default NotFound;