import React from 'react'
import Row from '../row'
import {withRouter} from "react-router-dom";
import {PersonDetails, PeopleList} from "../sw-components";

const PeoplePage = ({selectedId = 1, history}) => {
	return (
		<Row
			left={<PeopleList selectId={(id) => history.push(id)}/>}
			right={<PersonDetails itemId={selectedId}/>}
		/>
	)
}
export default withRouter(PeoplePage);