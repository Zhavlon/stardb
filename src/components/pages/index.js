import PeoplePage from './people-page'
import PlanetsPage from './planets-page'
import StarshipPage from './starships-page'
import LoginPage from './login-page'
import SecretPage from './secret-page'
import NotFound from './404-page'

export {PeoplePage, PlanetsPage, StarshipPage, SecretPage, LoginPage, NotFound}