import React from 'react';

const LoginPage = ({setAuth}) => {
	return (
		<div>
			<button
			onClick={setAuth}>Login</button>
		</div>
	);
};

export default LoginPage;