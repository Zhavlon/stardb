import React, {useState} from 'react';
import Header from '../header';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import RandomPlanet from '../random-planet';
import {PeoplePage, PlanetsPage, StarshipPage, SecretPage, LoginPage, NotFound} from "../pages";
import {StarshipDetails} from "../sw-components";
import {Provider} from '../swapi-context/swapi-context';
import SwapiService from "../../services/swapi-service";


import './app.css';

const App = () => {
	const [isAuth, setAuth] = useState(false)

	return (
		<div>
			<Provider value={SwapiService}>
				<Router>
					<Header/>
					<RandomPlanet/>

					<Switch>
						<Route path='/' exact render={() => <h1>Main page</h1>}/>
						<Route path='/people/:id?' render={({match}) => {
							const {id: selectedId} = match.params;
							return <PeoplePage selectedId={selectedId}/>
						}}/>
						<Route path='/planets/' component={PlanetsPage}/>

						<Route path='/starships/' exact component={StarshipPage}/>
						<Route path='/starships/:id' render={({match}) => {
							const {id: itemId} = match.params
							return <StarshipDetails itemId={itemId}/>
						}}/>

						<Route path='/secrets/' render={() => {
							return <SecretPage isAuth={isAuth}/>
						}}/>
						<Route path='/login' render={() => {
							return <LoginPage setAuth={() => setAuth(!isAuth)}/>
						}}/>
						<Route component={NotFound}/>

					</Switch>

				</Router>
			</Provider>
		</div>
	);
};

export default App;